require('chai');

describe('Object', function(){
  it('should work with JSON', function(){
    var value1 = 'value1';
    var value2 = 'value2';
    var object =  {'key1': value1, 'key2': value2};
    var JSONObject = JSON.stringify(object);
    var copy = JSON.parse(JSONObject);

    copy.should.eql(object);
  });
});

describe('Array', function(){
  it('should work with JSON', function(){
    var value1 = 'value1';
    var value2 = 'value2';
    var array =  [value1, value2];
    var JSONArray = JSON.stringify(array);
    var copy = JSON.parse(JSONArray);

    copy.should.eql(array);
  });
});
